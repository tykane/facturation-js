let today = new Date();
let date = today.getDay() + '/' + today.getMonth() + '/' + today.getFullYear();
var dateSpan = document.querySelector("#facture-date");
dateSpan.textContent = date;

let companies = [
    {id: 1, name: 'Google'},
    {id: 2, name: 'Apple'},
    {id: 3, name: 'Facebook'},
    {id: 4, name: 'Amazon'},
]

var companySelect = document.getElementById("company-select");
Array.from(companies).forEach(company => {
    var option = document.createElement("option");
    option.text = company.name;
    option.id = company.id;
    companySelect.add(option);
})

let types = ['Service', 'Jour', 'Heure'];
var lineNumber = 0;

function createNewFactureLine() {
    lineNumber++;
    let typeSelect = this.createElementWithClass('select', null, `custom-select type-select`);
    Array.from(types).forEach(type => {
        var option = document.createElement("option");
        option.text = type;
        typeSelect.add(option);
    })
    let description = this.createElementWithClass('textarea', null, `description`);

    let quantity = this.createElementWithClass('input', 'number', `quantity`);
    quantity.setAttribute('value', 0)
    quantity.addEventListener('change', () => updateDisplayPrice())

    let price = this.createElementWithClass('input', 'number', `price`);
    price.setAttribute('value', 0)
    price.addEventListener('change', () => updateDisplayPrice())

    let ht = this.createElementWithClass('span', null, `ht`);
    ht.innerText = '0€';
    let ttc = this.createElementWithClass('span', null, `ttc`);
    ttc.innerText = '0€';

    let deleteButton = this.createElementWithClass('button', null, `btn btn-danger delete-button${lineNumber}`);
    deleteButton.setAttribute('type', 'button')
    deleteButton.innerHTML =
        '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">\n' +
        '  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>\n' +
        '  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>\n' +
        '</svg>'
    ;

    var table = document.querySelector("#facture-container");
    var newFactureRow = table.insertRow();
    newFactureRow.insertCell(0).appendChild(typeSelect);
    newFactureRow.insertCell(1).appendChild(description);
    newFactureRow.insertCell(2).appendChild(quantity);
    newFactureRow.insertCell(3).appendChild(price);
    newFactureRow.insertCell(4).appendChild(ht);
    newFactureRow.insertCell(5).appendChild(ttc);
    newFactureRow.insertCell(6).appendChild(deleteButton);
    newFactureRow.setAttribute('id', lineNumber)

    deleteButton.addEventListener('click', () => {
        deleteFactureLine(newFactureRow.id)
    })

    function updateDisplayPrice() {
        ht.innerText = (price.value * quantity.value);
        ttc.innerText = (price.value * quantity.value) + ((price.value * quantity.value) * 0.2);
        updateGlobalPrice();
    }

    function updateGlobalPrice() {
        var htElements = document.querySelectorAll('.ht');
        var htPrice = 0;
        Array.from(htElements).forEach(element => {
            htPrice = htPrice + parseFloat(element.innerHTML);
        })
        document.querySelector("#sous-total-ht").innerHTML = htPrice;

        var ttcElements = document.querySelectorAll('.ttc');
        var ttcPrice = 0;
        Array.from(ttcElements).forEach(element => {
            ttcPrice = ttcPrice + parseFloat(element.innerHTML);
        })

        var sousTotalTTC = document.querySelector("#sous-total-ttc");
        sousTotalTTC.innerHTML = ttcPrice;

        document.querySelector("#total").innerHTML = sousTotalTTC.innerHTML.valueOf();
    }
}

function deleteFactureLine(line) {
    document.querySelector("#facture-container").deleteRow(line);
}

createNewFactureLine();

function createElementWithClass(elementName, elementType, className) {
    let element = document.createElement(elementName);
    element.setAttribute('class', className);
    if (elementType != null)
        element.setAttribute('type', elementType)
    return element;
}

document.querySelector('#add-line-button').addEventListener('click', () => {
    createNewFactureLine()
})

function createFacture() {
    var table = document.querySelector("#facture-container");
    var facture = {
        client: null,
        lines: [],
        totalHT: null,
        totalTTC: null
    };

    Array.from(table.rows).forEach(row => {
        let line = {
            type: null,
            description: null,
            quantity: null,
            price: null,
            ht: null,
            ttc: null
        }
        Array.from(row.cells).forEach(cell => {
            // cell.getElementsByClassName('type-select') etc
        })
        facture.lines.push(line);
    })

    facture.totalHT = parseFloat(document.querySelector("#sous-total-ht").innerHTML);
    facture.totalTTC = parseFloat(document.querySelector("#sous-total-ttc").innerHTML);

    facture.client = document.querySelector("#company-select").value;
    console.log(facture)
}

document.querySelector('#create-button').addEventListener('click', () => {
    createFacture()
})

